import React, {useEffect, useMemo} from 'react';
import Header from '../Header/Header';
import Sites from '../widgets/Sites/Sites';
import Weather from '../widgets/Weather/Weather';
import Links from '../Tabs/Tabs';
import {Link} from 'react-router-dom';
import Icon from '../Icon';
import './MainDashboard.css';
import Map from '../widgets/Map/Map';
import SalesByDepartment from '../widgets/SalesByDepartment/SalesByDepartment';
import {observer, Provider} from 'mobx-react';
import {useMainDashboardStores} from '../../store';
import Spinner from '../common/Spinner';
import {useApolloClient} from 'react-apollo';
import CarIcon from '../widgets/CarCount/assets/car_icon.svg';
import Box from '../widgets/Box/Box';
import Format from '../../utils/Format';
import IndicatorIcon from '../widgets/CurrentSales/assets/indicator.svg';
import SalesDetails from '../widgets/SalesDetails/SalesDetails';
import MainDashboardStore from '../../store/MainDashboardStore';
import DashboardStore from '../../store/DashboardStore';

const DashboardBody: React.FC = observer(() => {
  const {dashboard} = useMainDashboardStores();

  useEffect(() => {
    // noinspection JSIgnoredPromiseFromCall
    dashboard.load();
  }, [dashboard]);

  if (dashboard.isLoading) return (
    <Spinner/>
  );
  if (dashboard.error) {
    return <div>{dashboard.error}</div>;
  }

  return (
    <div className="row">
      <div className="col-lg-6">
        <Sites/>
      </div>
      <div className="col-lg-6 d-flex flex-column">
        <div className="mt-4">
          <h5>Weather</h5>
          <Weather/>
        </div>
        <Map/>
      </div>

      {/*<div className="col-lg-4 mt-4">*/}
      {/*  <Box*/}
      {/*    title='Current Sales'*/}
      {/*    info={Format.money(36.92)}*/}
      {/*    image={IndicatorIcon}*/}
      {/*    handleClick={mainDashboard.switchToCurrentSales}*/}
      {/*    isOpen={mainDashboard.currentDetails === 'current-sales'}*/}
      {/*  />*/}
      {/*</div>*/}
      {/*<div className="col-lg-4 mt-4">*/}
      {/*  <Box*/}
      {/*    title="Car Count"*/}
      {/*    info={mainDashboard.carCountSummary.count.toString()}*/}
      {/*    image={CarIcon}*/}
      {/*    isOpen={false}*/}
      {/*  />*/}
      {/*</div>*/}
      {/*<div className="col-lg-4 mt-4">*/}
      {/*</div>*/}

      {/*<div className={mainDashboard.currentDetails === 'current-sales' ? 'col-lg-12' : 'd-none'}>*/}
      {/*  <SalesDetails arrowPosition="left"/>*/}
      {/*</div>*/}

      {/*<div className="col-lg-6">*/}
      {/*  <SalesByDepartment/>*/}
      {/*</div>*/}
      <div className="col-lg-6">
      </div>
    </div>
  );
});

const options = {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'};

const date = () => (new Date()).toLocaleDateString('en-US', options);

const MainDashboard: React.FC = () => {
  const client = useApolloClient();

  const stores = useMemo(() => ({
    dashboard: new MainDashboardStore(client),
    //authStore: props.auth
  }), [client]);

  return (
    <div className="MainDashboard">
      <Header/>
      <div className="container">
        <div className="row">
          <div className="col-lg-6">
            <Links>
              <Link to="/">Dashboard</Link>
              {/*<Link to="/settings">Page settings</Link>*/}
              <Link to="/users">Users</Link>
            </Links>
          </div>
          <div className="col-lg-6">
            <div className="current-date">
              <Icon name="icon-calendar-icon" color="blue"/> <span className="title">{date()}</span>
            </div>
          </div>
        </div>
        <Provider {...stores}>
          <DashboardBody/>
        </Provider>
      </div>
    </div>
  );
};


export default MainDashboard;