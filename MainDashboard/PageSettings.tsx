import React from 'react';
import Header from '../Header/Header';
import Links from '../Tabs/Tabs';
import {Link} from 'react-router-dom';

const PageSettings: React.FC = () => {
  return (
    <div className="MainDashboard">
      <Header/>
      <div className="container">
        <div className="row">
          <div className="col-lg-6">
            <Links>
              <Link to="/">Dashboard</Link>
              <Link to="/settings">Page settings</Link>
              <Link to="/users">Users</Link>
            </Links>
          </div>
        </div>
      </div>
    </div>
  );
};


export default PageSettings;