<?php

declare(strict_types=1);

namespace common\models\job;

use common\yii\db\ActiveRecord;
use common\yii\helpers\DateHelper;
use common\yii\validators\CompareValidator;
use DateInterval;
use DateTime;
use Yii;

/**
 * Class JobStatus
 *
 * @package common\models\job
 *
 * @property string $id
 * @property string $name
 * @property string $description
 * @property int $status
 * @property int $in_queue
 * @property string $message
 * @property DateTime $started_at
 * @property DateTime $stopped_at
 */
class JobStatus extends ActiveRecord {

	/**
	 * Нет в очереди
	 * @var int
	 */
	const STATUS_NOT_PUSHED = 0;

	/**
	 * Выполняется в данный момент
	 * @var int
	 */
	const STATUS_WORKING = 1;

	/**
	 * В очереди, но не выполняется
	 * @var int
	 */
	const STATUS_NOT_WORKING = 2;

	/**
	 * Завершен с ошибкой
	 * @var int
	 */
	const STATUS_ERROR = 3;

	const ATTR_NAME = 'name';

	const ATTR_STATUS = 'status';

	const ATTR_DESCRIPTION = 'description';

	const ATTR_MESSAGE = 'message';

	const ATTR_STARTED_AT = 'started_at';

	const ATTR_STOPPED_AT = 'stopped_at';

	const ATTR_IN_QUEUE = 'in_queue';

	/**
	 * @var int
	 */
	const CONFIG_STRING_MAX = 255;

	/**
	 * Название таблицы в бд
	 *
	 * @return string
	 */
	public static function tableName(): string {
		return 'job_status';
	}

	/**
	 * Создание или получение записи статистики джоба, если она уже есть
	 *
	 * @param string $name
	 *
	 * @return static
	 */
	public static function getByName(string $name): self {
		$jobStatus = static::findOne([static::ATTR_NAME => $name]);
		return $jobStatus ?? static::createNew($name);

	}

	/**
	 * Создание новой записи
	 *
	 * @param string $name
	 *
	 * @return static
	 */
	private static function createNew(string $name): self {
		$jobStatus = new static();
		$jobStatus->name = $name;
		$jobStatus->status = static::STATUS_NOT_PUSHED;
		return $jobStatus;
	}

	/**
	 * {@inheritDoc}
	 */
	public function rules() {
		return [
			[[static::ATTR_NAME, static::ATTR_DESCRIPTION, static::ATTR_STATUS, static::ATTR_MESSAGE, static::ATTR_STARTED_AT, static::ATTR_STOPPED_AT, static::ATTR_IN_QUEUE], 'safe'],
			[[static::ATTR_NAME], 'required'],
			[static::ATTR_IN_QUEUE, CompareValidator::class, CompareValidator::ATTR_COMPARE_VALUE => 0, CompareValidator::ATTR_OPERATOR => '>=', 'type' => CompareValidator::TYPE_NUMBER],
			[[static::ATTR_NAME, static::ATTR_DESCRIPTION, static::ATTR_MESSAGE], 'string'],
		];
	}

	/**
	 * {@inheritDoc}
	 */
	public function attributeLabels() {
		return [
			static::ATTR_NAME        => Yii::t('common', 'Имя джоба'),
			static::ATTR_DESCRIPTION => Yii::t('common', 'Краткое описание'),
			static::ATTR_STARTED_AT  => Yii::t('common', 'Последний запуск'),
			static::ATTR_STOPPED_AT  => Yii::t('common', 'Последняя остановка'),
			static::ATTR_STATUS      => Yii::t('common', 'Статус'),
		];
	}

	/**
	 * Длительность последней или текущей работы джоба. Может отсутствовать
	 *
	 * @return DateInterval|null
	 */
	public function getDuration(): ?DateInterval {
		// никогда не стартовал - нисколько не длился
		if ($this->started_at === null) {
			return null;
		}
		$startedAt = DateHelper::utc($this->started_at);

		// если никогда не останавливался или работает в данный момент или время старта больше времени завершения - длится и сейчас
		if ($this->stopped_at === null || $this->isWorking() || $this->started_at > $this->stopped_at) {
			return $startedAt->diff(DateHelper::nowUTC());
		}
		// длился от времени начала до времени завершения
		return $startedAt->diff(DateHelper::utc($this->stopped_at));
	}

	/**
	 * Работает ли джоб в данный момент
	 *
	 * @return bool
	 */
	public function isWorking(): bool {
		return $this->status === static::STATUS_WORKING;
	}

	/**
	 * Регистрация события добавления джоба в очередь
	 */
	public function registerPush(): void {
		if (!$this->isInQueue()) {
			$this->setStatus(static::STATUS_NOT_WORKING);
		}
		$this->in_queue++;
	}

	/**
	 * Имеются ли джобы этого типа в очереди
	 *
	 * @return bool
	 */
	public function isInQueue(): bool {
		return (int)$this->in_queue > 0;
	}

	/**
	 * Внутренний метод для смены статуса. Позволяет не стирать предыдущее сообщение, если его нет.
	 *
	 * @param int         $status
	 * @param string|null $message текст сообщения. Устанавливается только, если не null. По умолчанию null (не устанавливается)
	 */
	private function setStatus(int $status, ?string $message = null): void {
		$this->status = $status;

		if ($message !== null) {
			$this->message = $message;
		}
	}

	/**
	 * Регистрация события завершения джоба с ошибкой
	 *
	 * @param string|null $message
	 */
	public function registerError(?string $message = null): void {
		$this->setStatus(static::STATUS_ERROR, $message);
		$this->stoppedNow();
	}

	/**
	 * Обработка остановки джоба: регистрация времени остановки и уменьшение очереди
	 */
	private function stoppedNow(): void {
		$this->stopped_at = DateHelper::nowUTC()->format('Y-m-d H:i:s');

		if ($this->isInQueue()) {
			$this->in_queue--;
		}
	}

	/**
	 * Регистрация события штатного завершения работы джоба
	 */
	public function registerStop(): void {
		$this->stoppedNow();
		if ($this->isInQueue()) {
			$this->setStatusClearMessage(static::STATUS_NOT_WORKING);
		} else {
			$this->setStatusClearMessage(static::STATUS_NOT_PUSHED);
		}
	}

	/**
	 * Устанавливает статус и сбрасывает текущее сообщение об ошибке
	 *
	 * @param int $status
	 */
	private function setStatusClearMessage(int $status): void {
		$this->setStatus($status, '');
	}

	/**
	 * Регистрация события запуска джоба
	 */
	public function registerStart(): void {
		$this->setStatus(static::STATUS_WORKING);
		$this->startedNow();
	}

	/**
	 * Обработка запуска джоба
	 */
	private function startedNow(): void {
		$this->started_at = DateHelper::nowUTC()->format('Y-m-d H:i:s');
	}

	/**
	 * Текстовое представление текущего статуса
	 *
	 * @return string
	 */
	public function getStatusText(): string {
		if ($this->hasError()) {
			return static::statusList()[$this->status].': '.$this->message;
		}
		return static::statusList()[$this->status];
	}

	/**
	 * Завершился ли последний раз джоб с ошибкой
	 *
	 * @return bool
	 */
	public function hasError(): bool {
		return $this->status === static::STATUS_ERROR;
	}

	/**
	 * Текстовое представление значений статуса
	 *
	 * @return array
	 */
	public static function statusList(): array {
		return [
			static::STATUS_NOT_PUSHED  => 'Не запущен',
			static::STATUS_WORKING     => 'В работе',
			static::STATUS_NOT_WORKING => 'Ожидает',
			static::STATUS_ERROR       => 'Ошибка'
		];
	}

	/**
	 * Вернуть статистику к исходному состоянию (полная очистка очереди, кроме случая, когда джоб запущен)
	 */
	public function toInitialState(): void {
		$this->setStatus(static::STATUS_NOT_PUSHED);
		$this->in_queue = 0;
	}

	/**
	 * Изменение описания джоба
	 *
	 * @param string $description
	 */
	public function setDescription(string $description): void {
		$description = substr($description, 0, static::CONFIG_STRING_MAX - 1);
		if ($this->description !== $description) {
			$this->description = $description;
		}
	}
}