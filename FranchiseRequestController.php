<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\School;
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Services\TimeZonesServices;

class FranchiseRequestController extends Controller
{
    /**
     * FranchiseRequestController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('role:boss|admin|teacher');
    }

    /**
     * @return View
     */
    public function index()
    {
        $user = auth()->user();
        if ($user->level === 50) {
            $schools = School::mySchool()->get();
        } else {
            $schools = $user->schools;
        }
        return view('pages.admin.franchise-request', [
            'timezoneList' => TimeZonesServices::timeZoneList(),
            'schools' => $schools
        ]);
    }
}