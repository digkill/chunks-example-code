<?php

namespace common\components\bl;

use common\components\bl\ledger\GuardianEventLedger;
use common\exception\LogicException;
use common\helpers\ArrayHelper;
use common\models\Board;
use common\models\BoardFees;
use common\models\Cart;
use common\models\EventProduct;
use common\models\Grade;
use common\models\GuardianCard;
use common\models\ProductPhoto;
use common\models\ProductVariation;
use common\models\PurchaseTransaction;
use common\models\School;
use common\models\Student;
use common\payments\bambora\ResponsePayment;
use Yii;
use yii\base\Exception;
use yii\db\Query;

class CartManager
{
    const CUSTOMER_TYPE_GUARDIAN = 0x1;
    public $customerId;

    private $cardTokens = [];

    public static function create(int $guardianId)
    {
        return new self($guardianId);
    }

    public function __construct(int $guardianId)
    {
        $this->customerId = $guardianId;
    }

    /**
     * @param EventProduct $eventProduct
     * @param array $counters
     * @param int $quantity
     * @throws LogicException
     */
    private function checkQuantity(EventProduct $eventProduct, $counters, int $quantity)
    {
        $inCart = ArrayHelper::remove($counters, 'in_cart', 0);
        $purchased = ArrayHelper::remove($counters, 'purchased', 0);

        $total = $inCart + $purchased;
        $min = max(1, $eventProduct->min_quantity - $total);
        if ($quantity < $min) {
            throw new LogicException("You can't purchase less than $min product(s)");
        }
        if ($eventProduct->max_quantity === 0) {
            return;
        }
        $max = $eventProduct->max_quantity - $total;
        if ($max <= 0) {
            throw new LogicException("You've already purchased max quantity of product");
        }
        if ($quantity > $max) {
            throw new LogicException("You can't purchase greater than $max product(s)");
        }
    }

    /**
     * @param int $studentId
     * @param EventProduct $eventProduct
     * @param int $variationId
     * @param $price
     * @param int $quantity
     * @param BoardFees $boardFees
     * @throws LogicException
     */
    public function addToCart(int $studentId, EventProduct $eventProduct, $variationId, $price, int $quantity)
    {
        if ($quantity <= 0) {
            throw new LogicException("wrong quantity");
        }
        $counters = $this->getPurchasedQuantity($studentId, $eventProduct);
        $this->checkQuantity($eventProduct, $counters, $quantity);

        $cartId = ArrayHelper::remove($counters, 'cart_id');

        if (!empty($cartId)) {
            $inCart = ArrayHelper::remove($counters, 'in_cart', 0);
            Cart::updateAll([
                'quantity' => $inCart + $quantity,
                'total' => $eventProduct->applyTax($price) * $quantity,
            ], ['id' => $cartId]);
            return;
        }

        $cart = new Cart($eventProduct->getAttributes([
            'doc_event_id',
            'product_id',
        ]));


        $cart->price = $price;
        $cart->product_variation_id = $variationId;
        $cart->guardian_id = $this->customerId;
        $cart->student_id = $studentId;
        $cart->quantity = $quantity;
        $cart->tax = $eventProduct->product->tax;
        $cart->total = $eventProduct->applyTax($price) * $quantity;
        $cart->expiry_date = (new \DateTime('next week'))->format('Y-m-d H:i:s');
        $cart->save(false);
    }

    /**
     * @param $eventId
     * @param $studentId
     * @param $products
     * @param BoardFees $boardFees
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function addToCartMany($eventId, $studentId, $products)
    {
        $eventProducts = EventProduct::find()->where([
            'doc_event_id' => $eventId,
        ])->all();

        $variations = array_filter(array_column($products, 'variation'), 'strlen');

        $prices = ProductVariation::find()
            ->select(['id', 'price'])
            ->where(['in', 'id', $variations])
            ->asArray()
            ->all();

        $prices = ArrayHelper::map($prices, 'id', 'price');

        $transaction = Yii::$app->db->beginTransaction();
        foreach ($eventProducts as $eventProduct) {
            $item = ArrayHelper::remove($products, $eventProduct->id, []);
            $quantity = (int)ArrayHelper::remove($item, 'quantity', 0);
            $variation = ArrayHelper::remove($item, 'variation', null);
            if (empty($quantity)) {
                continue;
            }
            try {
                $this->addToCart(
                    $studentId,
                    $eventProduct,
                    $variation,
                    empty($variation) ? $eventProduct->price : $prices[$variation],
                    $quantity);
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
        $transaction->commit();
    }

    private function getProductQuantityQuery(int $studentId, EventProduct $eventProduct)
    {
        return (new Query())
            ->select('quantity')
            ->from('cart')
            ->where([
                'guardian_id' => $this->customerId,
                'student_id' => $studentId,
                'doc_event_id' => $eventProduct->doc_event_id,
                'product_id' => $eventProduct->product_id,
            ]);
    }

    public function getPurchasedQuantity(int $studentId, EventProduct $eventProduct)
    {
        $ldgQuery = (new Query())
            ->select([
                'product_id',
                'sum(quantity) purchased'
            ])
            ->from('ldg_event_activity_purchase')
            ->where([
                'student_id' => $studentId,
                'doc_event_id' => $eventProduct->doc_event_id,
                'product_id' => $eventProduct->product_id,
            ])
            ->groupBy('product_id');

        return (new Query())
            ->select([
                'c.id cart_id',
                'c.quantity in_cart',
                'l.purchased',
            ])
            ->from([
                'c' => $this->getProductQuantityQuery($studentId, $eventProduct)
                    ->addSelect(['id', 'product_id']),
            ])
            ->join('FULL OUTER JOIN', ['l' => $ldgQuery], 'c.product_id = l.product_id')
            ->one();
    }

    public function generateOrderId()
    {
        return sprintf('%s-%s-%s',
            self::CUSTOMER_TYPE_GUARDIAN,
            $this->customerId,
            time()
        );
    }

    /**
     * @return float
     */
    public function getAmount()
    {

        $studentId = array_column($this->getItems(), 'student_id');
        $studentId = array_shift($studentId);
        $sum = array_sum(array_column($this->getItems(), 'total'));

        $fees = $this->getBoardFees($studentId);

        if (!empty($fees)) {
            foreach ($fees as $key => $fee) {
                if ($fee["min_amount"] <= $sum && $fee["max_amount"] >= $sum) {
                    $sum += $this->getFees();
                }
            }
        }

        return round($sum, 2, PHP_ROUND_HALF_UP);
    }

    public function getTotal()
    {
        return $this->getNet() + $this->getTax() + $this->getFees();
    }

    public function getFees()
    {
        $studentId = array_column($this->getItems(), 'student_id');
        $studentId = array_shift($studentId);
        $sum = 0;
        $fees = $this->getBoardFees($studentId);


        if (!empty($fees)) {
            foreach ($fees as $key => $fee) {
                if ($fee["min_amount"] <= $sum && $fee["max_amount"] >= $sum) {
                    $sum = $this->getNet() * $fee["variable_percent"] / 100 + $fee["fixed_amount"];
                }
            }
        }

        return round($sum, 2, PHP_ROUND_HALF_UP);
    }

    public function getTax()
    {

        $total = 0;

        foreach ($this->getItems() as $key => $item) {
            $total += $item['price'] * $item['quantity'];
        }
        $tax = array_sum(array_column($this->getItems(), 'total'));
        $tax = $total - $tax;
        $tax = abs($tax);
        return number_format($tax, 2);
    }

    /**
     * @return float
     */
    public function getNet()
    {
        $net = 0;
        foreach ($this->getItems() as $key => $item) {
            $net += $item['price'] * $item['quantity'];
        }
        return number_format($net, 2);
    }

    public function getTotalCount()
    {
        static $count;
        if (empty($count)) {
            $count = (new \yii\db\Query())
                ->select('SUM(quantity)')
                ->from('cart')
                ->where(['guardian_id' => $this->customerId])
                ->scalar();
        }
        return $count;
    }

    public function getProductQuantity(int $studentId, EventProduct $eventProduct)
    {
        return $this->getProductQuantityQuery($studentId, $eventProduct)->scalar();
    }

}