import React, {useEffect, useMemo} from 'react';
import Header from '../Header/Header';
import Weather from '../widgets/Weather/Weather';
import './Dashboard.css';

import CurrentSales from '../widgets/CurrentSales/CurrentSales';
import CarDetails from '../widgets/CarDetails/CarDetails';
import LaborDetails from '../widgets/LaborDetails/LaborDetails';
import SalesDetails from '../widgets/SalesDetails/SalesDetails';


import {observer, Provider} from 'mobx-react';
import {useDashboardStores} from '../../store';
import Spinner from '../common/Spinner';

import SalesByDepartment from '../widgets/SalesByDepartment/SalesByDepartment';
import DashboardHeader from '../widgets/DashboardHeader/DashboardHeader';
import OpenDay from '../widgets/OpenDay/OpenDay';
import LaborEmployeeDetails from '../widgets/LaborEmployeeDetails/LaborEmployeeDetails';
import {useApolloClient} from 'react-apollo';
import DashboardStore, {CurrentDetails} from '../../store/DashboardStore';
import CarCount from '../widgets/CarCount/CarCount';
import Labor from '../widgets/Labor/Labor';
import LaborByEmployeeSummary from '../widgets/LaborByEmployeeSummary/LaborByEmployeeSummary';
import {RouteComponentProps} from 'react-router';
import withMobile from '../hoc/WIthMobile';
import DashboardRow from '../common/mobile/DashboardRow';
import Cloud from '../common/Cloud';
import Graph from '../widgets/Gaph/Graph';
import Card from '../Card';
import Format from '../../utils/Format';
import CurrentStaff from '../widgets/CurrentStaff/CurrentStaff';

const detailsIsVisible = (expected: CurrentDetails, actual?: CurrentDetails) => {
  return expected === actual ? 'col-12' : 'd-none';
};

const Body: React.FC = observer(() => {
  const {dashboard} = useDashboardStores();

  if (dashboard.isLoading) return (
    <Spinner/>
  );
  if (dashboard.error) {
    return <div>{dashboard.error}</div>;
  }

  return (
    <div className="container">
      <DashboardHeader/>
      <div className="row block-row">
        <div className="col-md-6 col-lg-4">
          <CarCount
            isOpen={dashboard.currentDetails === 'car-count'}
            handleClick={dashboard.switchToCarCount}
          />
        </div>
        <div className="col-md-6 col-lg-3">
          <OpenDay/>
        </div>
        <div className="col-md-6 col-lg-5 d-none d-lg-block">
          <Weather/>
        </div>
        <div className={detailsIsVisible('car-count', dashboard.currentDetails)}>
          <Cloud arrowPosition="left">
            <div className='row'>
              <div className='col-sm-7 position-static'>
                <h5>Details</h5>
                <CarDetails/>
              </div>
              <div className='col-sm-5'>
                <Graph width={322} height={183} data={dashboard.carCount.items} xAxis={'time'} yAxis={'count'}
                       title={'Hourly Car Count'}/>
              </div>
            </div>
          </Cloud>
        </div>
      </div>
      <div className="row">
        <div className='col-12 col-md-7'>
          <div className="row ">
            <div className='col-7'>
              <Labor
                isOpen={dashboard.currentDetails === 'labor'}
                handleClick={dashboard.switchToLabor}
              />
            </div>
            <div className='col-12 col-md-5'>
              <CurrentSales
                isOpen={dashboard.currentDetails === 'current-sales'}
                handleClick={dashboard.switchToCurrentSales}
              />
            </div>
            <div className={detailsIsVisible('current-sales', dashboard.currentDetails)}>
              <Cloud arrowPosition="right">
                <h5>Details</h5>
                <SalesDetails/>
              </Cloud>
            </div>
            <div className={detailsIsVisible('labor', dashboard.currentDetails)}>
              <Cloud arrowPosition="left">
                <h5>Details</h5>
                <LaborDetails/>
              </Cloud>
            </div>

            <div className='col-md-7'>
              <LaborByEmployeeSummary
                isOpen={dashboard.currentDetails === 'labor-by-employee'}
                handleClick={dashboard.switchToLaborByEmployee}
              />
            </div>
          </div>
        </div>

        <div className='col-12 col-md-5'>
          <Card className="SalesByDepartment shadow">
            <div className="card-body">
              <h5>Sales By Department</h5>
              <SalesByDepartment/>
            </div>
          </Card>
        </div>

        <div className={detailsIsVisible('labor-by-employee', dashboard.currentDetails)}>
          <Cloud arrowPosition="left">
            <div className="row">
              <div className="col-6 position-static">
                <h5>Details</h5>
                <LaborEmployeeDetails/>
              </div>
              <div className="col-6">
                <CurrentStaff/>
              </div>
            </div>
          </Cloud>
        </div>
      </div>
    </div>
  );
});

const MobileBody: React.FC = observer(() => {
  const {dashboard} = useDashboardStores();
  const {carCountSummary, laborSummary, laborByEmployeeSummary, currentSalesSummary} = dashboard;

  if (dashboard.isLoading) return (
    <Spinner/>
  );
  if (dashboard.error || (!carCountSummary || !laborSummary || !laborByEmployeeSummary || !currentSalesSummary)) {
    return <div>{dashboard.error}</div>;
  }

  return (
    <div className="container">
      <DashboardHeader/>
      <div className="row">
        <div className="col-12">
          <OpenDay/>
          <DashboardRow title="Car count" trend={3} value={carCountSummary.count.toString()} icon="icon">
            <CarDetails/>
            <Graph width={322} height={183} data={dashboard.carCount.items} xAxis={'time'} yAxis={'count'}
                   title={'Hourly Car Count'}/>
          </DashboardRow>
          <DashboardRow title="Current sales" trend={3} value={Format.money(currentSalesSummary.value)} icon="icon">
            <SalesDetails/>
          </DashboardRow>
          <DashboardRow title="Labor" trend={3} value={Format.money(laborSummary.value)} icon="icon">
            <LaborDetails/>
          </DashboardRow>
          <DashboardRow title="Labor by Employee" trend={3} value={Format.money(laborByEmployeeSummary.value)}
                        icon="icon">
            <LaborEmployeeDetails/>
            <CurrentStaff/>
          </DashboardRow>
          <DashboardRow title="Sales by Department" trend={3} icon="icon" underline={false}>
            <SalesByDepartment/>
          </DashboardRow>
          <Weather/>
        </div>
      </div>
    </div>
  );
});

const DashboardBody = withMobile({
  mobileComponent: MobileBody,
})(Body);

interface Params {
  id: string
}

const Dashboard: React.FC<RouteComponentProps<Params>> = observer((props: RouteComponentProps<Params>) => {
  const id = props.match.params.id;

  const client = useApolloClient();

  const stores = useMemo(() => ({
    dashboard: new DashboardStore(client, id),
  }), [client]);

  useEffect(() => {
    // noinspection JSIgnoredPromiseFromCall
    stores.dashboard.load();

  }, [client]);

  return (
    <div className="Dashboard">
      <Header/>
      <Provider {...stores}>
        <DashboardBody/>
      </Provider>
    </div>
  );
});

export default Dashboard;