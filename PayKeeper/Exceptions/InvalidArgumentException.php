<?php

namespace App\Services\PayKeeper\Exceptions;

use Exception;
use Throwable;

class InvalidArgumentException extends Exception
{
    /**
     * @var string
     */
    protected $message;

    /**
     * InvalidArgumentException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = "", int $code = 400, Throwable $previous = null)
    {
        if (strlen($message) === 0) {
            $message = "Pay Keeper - error!";
        }
        parent::__construct($message, $code, $previous);

        $this->message = $message;
    }


    /**
     * Report the exception.
     *
     * @return void
     */
    public function report()
    {
        //
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        if ($request->ajax() || $request->header('Accept') == 'application/json') {
            return response()->json([
                'errors' => $this->message
            ], 400);
        }
        abort(404);
    }
}
