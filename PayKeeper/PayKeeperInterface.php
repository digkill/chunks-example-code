<?php

namespace App\Services\PayKeeper;

use App\Services\PayKeeper\Exceptions\InvalidArgumentException;

interface PayKeeperInterface
{
    public function setAuthParameters();
    public function apiGetToken();
    public function apiPostBill(array $property);
    public function apiPostSendBill(int $invoiceId);
    public function apiGetBill(int $invoiceId);
    public function apiGetPayment(int $paymentId);
    public function apiGetPaymentRefund(int $paymentId);
    public function apiGetSetting();
    public function apiGetUserSetting();
    public function apiPostSettingChange(string $name, string $value);
    public function callback(array $parameters, string $secret_seed);
}