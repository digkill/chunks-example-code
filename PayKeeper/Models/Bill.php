<?php

namespace App\Services\PayKeeper\Models;


class Bill
{
    /**
     * Уникальный номер созданного счёта
     * @var integer
     * @example '3154862'
     */
    public $invoice_id;
    /**
     * Уникальный номер созданного счёта
     * @var integer
     * @example '3154862'
     */
    public $id;
    /**
     * Статус счета
     * @var string
     * @example "created" OR "" OR ""
     */
    public $status;
    /**
     * Сумма
     * @var float
     * @example '35000.00'
     */
    public $pay_amount;
    /**
     * Клиент
     * @var string
     * @example "Стамакс Юрий"
     */
    public $clientid;
    /**
     * Идентификатор заказа
     * @var string
     * @example "145"
     */
    public $orderid;
    /**
     * Идентификатор платежа
     * @var integer|null
     * @example '245875' or NULL
     */
    public $paymentid;
    /**
     * Наименование услуги
     * @var string
     * @example 'Subscription - 8 days',
     */
    public $service_name;
    /**
     * Електронная почта
     * @var string
     * @example 'example@example.com'
     */
    public $client_email;
    /**
     * Телефон клиента
     * @var string
     * @example '+89160000001'
     */
    public $client_phone;
    /**
     * Счёт действует до Дата в формате YYYY-MM-DD HH:MM:SS
     * @var string|null
     * @example '2019-06-05 00:00:00'
     */
    public $expiry_datetime;
    /**
     * Дата создания счёта - Дата в формате YYYY-MM-DD HH:MM:SS
     * @var string
     * @example '2019-06-04 11:08:30'
     */
    public $created_datetime;
    /**
     * Дата оплаты - Дата в формате YYYY-MM-DD HH:MM:SS
     * @var string|null
     * @example '2019-06-04 12:08:30' or NULL
     */
    public $paid_datetime;
    /**
     * HTML-код предпросмотра e-mail сообщения, которое будет выслано клиенту
     * @var string
     * @example '<HTML> .... </HTML>'
     */
    public $invoice;
    /**
     * Текст ошибки
     * @var string
     */
    protected $msg;
    /**
     * Результат выволнения
     * @var string
     * @example 'success' OR 'fail'
     */
    protected $result;

    /**
     * Bill constructor.
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        // set values
        foreach ($parameters as $key => $parameter) {
            if (property_exists($this, $key)) {
                if ($key === 'invoice_id') {
                    $this->id = $parameter;
                    $this->result = 'success';
                }
                if ($key === 'id') {
                    $this->invoice_id = $parameter;
                    $this->result = 'success';
                }

                $this->{$key} = $parameter;
            }
        }
    }

    /**
     * Если результат "Успех"
     * @return bool
     */
    public function isSuccess()
    {
        return $this->result == 'success';
    }

    /**
     * Если результат "Збой"
     * @return bool
     */
    public function isFail()
    {
        return $this->result == 'fail';
    }

    /**
     * Получаем ответ с текстом ошибки
     * @return string
     */
    public function getMsg()
    {
        return $this->msg;
    }
}