<?php


namespace App\Services\PayKeeper\Models;


class PaymentRefund
{
    /**
     * Уникальный номер созданного счёта
     * @var integer
     * @example '3154862'
     */
    public $id;
    /**
     * Идентификатор платежа
     * @var integer
     * @example '124076'
     */
    public $payment_id;
    /**
     * Порядковый номер возврата по данному платежу
     * @var integer
     * @example '1'
     */
    public $refund_number;
    /**
     * Пользователь, инициировавший возврат
     * @var string
     * @example "admin"
     */
    public $user;
    /**
     * Сумма возврата
     * @var float
     * @example '9730.00'
     */
    public $amount;
    /**
     * Остаток после возврата
     * @var float
     * @example '30.00'
     */
    public $remainder;
    /**
     * Дата/Время проведения возврата
     * @var string
     * @example '2017-02-10 10:58:36',
     */
    public $datetime;
    /**
     * Статус возврата. Принимает значения «started», «done», «failed»
     * @var string
     * @example 'started' OR 'done' OR 'failed'
     */
    public $status;
    /**
     * Текст ошибки
     * @var string
     */
    protected $msg;
    /**
     * Результат выволнения
     * @var string
     * @example 'success' OR 'fail'
     */
    protected $result;

    /**
     * Bill constructor.
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        // set values
        foreach ($parameters as $key => $parameter) {
            if (property_exists($this, $key)) {
                if ($key === 'id') {
                    $this->result = 'success';
                }

                $this->{$key} = $parameter;
            }
        }
    }

    /**
     * Если результат "Успех"
     * @return bool
     */
    public function isSuccess()
    {
        return $this->result == 'success';
    }

    /**
     * Если результат "Збой"
     * @return bool
     */
    public function isFail()
    {
        return $this->result == 'fail';
    }

    /**
     * Получаем ответ с текстом ошибки
     * @return string
     */
    public function getMsg()
    {
        return $this->msg;
    }
}