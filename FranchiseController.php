<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Services\TimeZonesServices;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
class FranchiseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('level:100');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('pages.admin.franchise', [
            'statuses' => Admin::statusList(),
            'timezoneList' => TimeZonesServices::timeZoneList(),
        ]);
    }
}
