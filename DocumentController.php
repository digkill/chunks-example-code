<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Document;
use Illuminate\Support\Facades\Storage;

class DocumentController extends Controller
{
    /**
     * @var \Illuminate\Contracts\Filesystem\Filesystem
     */
    protected $storage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->storage = Storage::disk('docs');
    }

    /**
     * Download file
     *
     * @param string $p1
     * @param string $p2
     * @param string $name
     * @return \Illuminate\Http\RedirectResponse
     */
    public function showDocs($p1 = '', $p2 = '', $name = '')
    {
        $p1 = $p2 ? "$p1/" : "$p1";
        $p2 = $name ? "$p2/" : "$p2";

        $file = $p1 . $p2 . $name;

        if ($this->storage->exists($file)) {
            return $this->storage->download($file);
        }

        return redirect()->back();
    }
}
